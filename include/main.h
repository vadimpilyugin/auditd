#pragma once

#include <queue>
#include <thread>
#include <chrono>

template<typename T>
class ThreadsafeQueue {
  std::queue<T> queue_;
  mutable std::mutex mutex_;
  const int max_size_;
  bool is_closed_;

  // Moved out of public interface to prevent races between this
  // and pop().
  bool empty() const {
    return queue_.empty();
  }

 public:
  ThreadsafeQueue(int max_size = 0): max_size_(max_size) {}
  ThreadsafeQueue(const ThreadsafeQueue<T> &) = delete ;
  ThreadsafeQueue& operator=(const ThreadsafeQueue<T> &) = delete ;

  ThreadsafeQueue(ThreadsafeQueue<T>&& other) {
    std::lock_guard<std::mutex> lock(mutex_);
    queue_ = std::move(other.queue_);
  }

  virtual ~ThreadsafeQueue() { }

  unsigned long size() const {
    std::lock_guard<std::mutex> lock(mutex_);
    return queue_.size();
  }

  void close() {
    std::lock_guard<std::mutex> lock(mutex_);
    is_closed_ = true;
  }

  std::optional<T> pop() {
    std::lock_guard<std::mutex> lock(mutex_);
    if (queue_.empty()) {
      return {};
    }
    T tmp = queue_.front();
    queue_.pop();
    return tmp;
  }

  std::optional<T> pop_blocking() {
    using namespace std::chrono_literals;
    while (true) {
      {
        std::lock_guard<std::mutex> lock(mutex_);
        if (!queue_.empty()) {
          T tmp = queue_.front();
          queue_.pop();
          return tmp;
        }

        if (is_closed_) {
          return std::nullopt;
        }
      }
      std::this_thread::sleep_for(200ms);
    }
  }

  void push_blocking(const T &item) {
    using namespace std::chrono_literals;
    while (true) {
      {
        std::lock_guard<std::mutex> lock(mutex_);
        if (is_closed_) {
          throw std::runtime_error("writing to closed queue");
        }
        if (queue_.size() < max_size_) {
          queue_.push(item);
          return;
        }
      }
      std::this_thread::sleep_for(200ms);
    }
  }
};
