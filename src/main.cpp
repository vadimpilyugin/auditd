#include <iostream>
#include <fstream>
#include <sstream>

#include "nlohmann/json.hpp"
#include "boost/algorithm/string.hpp"
#include "boost/program_options.hpp"
#include "boost/regex.hpp"
#include "main.h"

std::optional<std::pair<std::string, std::string>> split(std::string s) {
    auto idx = s.find_first_of('=');
    if (idx != std::string::npos) {
        auto name = s.substr(0, idx);
        auto value = s.substr(idx + 1);
        return std::pair(name,value);
    }
    return std::nullopt;
}

void file_reader(std::shared_ptr<ThreadsafeQueue<nlohmann::json>> queue, std::string fn) {

    std::ifstream infile;
    infile.exceptions ( std::ofstream::failbit | std::ofstream::badbit );
    infile.open(fn);
    infile.exceptions ( std::ofstream::badbit );

    std::string line;
    while (std::getline(infile, line))
    {
        nlohmann::json j;
        std::vector<std::string> strs;

        boost::split(strs, line, boost::is_any_of("\t "));
        if (strs.size() < 2) {
            std::cerr << "line is too short: " << line << std::endl;
            continue;
        }

        // type=SYSCALL
        auto res = split(strs[0]);
        if (!res.has_value()) {
            std::cerr << "no type: " << strs[0] << std::endl;
            continue;
        }
        auto [name, value] = *res;
        j[name] = value;

        // msg=audit(1364481363.243:24287):
        res = split(strs[1]);
        if (!res.has_value()) {
            std::cerr << "no audit timestamp: " << strs[1] << std::endl;
            continue;
        }
        std::tie(name, value) = *res;

        boost::regex reg("audit\\(([^:]+):(\\d+)\\):");
        boost::smatch match;

        if (boost::regex_search(value, match, reg)) {
            if (match.size() < 3) {
                std::cerr << "could not parse audit timestamp " << value << std::endl;
            } else {
                j["ts"] = std::string(match[1].first, match[1].second);
                j["id"] = std::string(match[2].first, match[2].second);
            }
        }

        for (int i = 2; i < strs.size(); i++) {
            auto res = split(strs[i]);
            if (!res.has_value()) {
                std::cerr << "no '=' delimiter found: " << strs[i] << std::endl;
                continue;
            }
            auto [name, value] = *res;
            j["data"][name] = value;
        }

        queue->push_blocking(j);
    }

    queue->close();
}

void file_writer(std::shared_ptr<ThreadsafeQueue<nlohmann::json>> queue, std::string fn) {
    std::ofstream outfile;
    outfile.exceptions ( std::ofstream::failbit | std::ofstream::badbit );
    outfile.open(fn);
    outfile.exceptions ( std::ofstream::badbit );

    std::optional<nlohmann::json> res;
    while ((res = queue->pop_blocking()).has_value()) {
        auto js = *res;
        outfile << js << std::endl;
    }
}

int main(int argc, char const *argv[])
{
    std::cout << "Hello, world!\n";

    std::string infn("/var/log/audit/audit.log");
    std::string outfn("out.json");

    namespace po = boost::program_options;
    po::variables_map vm;
    po::options_description desc("Usage: ./main [options...]");
    desc.add_options()
    ("help", "produce help message")
    ("infile", po::value<std::string>(&infn), "Audit log file")
    ("outfile", po::value<std::string>(&outfn), "JSON output file")
    ;

    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << "\n";
        return 0;
    }

    if (vm.count("infile")) {
        infn = vm["infile"].as<std::string>();
    }

    if (vm.count("outfile")) {
        outfn = vm["outfile"].as<std::string>();
    }

    auto queue = std::make_shared<ThreadsafeQueue<nlohmann::json> >(50);
    std::thread input(file_reader, queue, infn);
    std::thread output(file_writer, queue, outfn);
    input.join();
    output.join();

    return 0;
}